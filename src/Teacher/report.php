<?php
include('header.php');
$page = 'report';
 ?>

<body>
  <?php include('navbar.php'); ?>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#showOrHide').hide();
      $.ajax({
        type: "POST",
        url: "CourseList.php",
        success: function(msg) {
          $('#sel_course_code').html(msg);
        }
      });

      $('#student_report_generate').click(function() {
        $('#showOrHide').show();
        $.ajax({
          type: "POST",
          url: "reportFile.php",
          data: {
            session: $('#session').val(),
            course: $('#sel_course_code').val(),
            marks: $('#Attendance-marks').val()
          },
          success: function(msg) {
            $('#showOrHide').show();
            $('#reportTable').html(msg);
          }
        });
        $.ajax({
          type: "POST",
          url: "reportSheet.php",
          data: {
            session: $('#session').val(),
            course: $('#sel_course_code').val()
          },
          success: function(msg) {
            $('#reportsheet').html(msg);
          }
        });
      });
    });
  </script>

  <section>
    <div id="Atten-Genarate-section" class="container">
      <div class="row">
        <div class="col form-group">
          <label for="">Session</label>
          <select id="session" class="form-control custom-select" name="" required>
            <?php
						$count = 4; // count will be get from database
						if ($count>8) {
							$count=8;
						}
						$currYear = date('Y');
						$prevYear = $currYear - 1;

						for ($i=0; $i < $count; $i++) {
							$session = ($prevYear - $i) . "-" . ($currYear - $i);
							echo "<option value='" . $session . "'>" . $session . "</option>";
						}
             ?>
          </select>
        </div>

        <div class="col">
          <label for="">Course</label>
          <select id="sel_course_code" class="form-control custom-select" name="" required>
            <?php //include('getDropdownCourse.php'); ?>

          </select>
        </div>
        <div class="col">
          <label for="">Attentance Marks</label>
          <input type="number" name="" value="" class="form-control" id="Attendance-marks" required>
        </div>
      </div>
      <div class="">
        <button id="student_report_generate" type="button" class="btn btn-info btn-lg btn-block mt-5" name="student-report-generate-button">Show Report</button>
      </div>
    </div>
  </section>

  <div class="container" id="showOrHide">
    <div class="border-primary  " id="reportsheet" style='text-align: center;'>
      <!-- calling reportSheet.php -->
    </div>
    <div class="row">
      <table class="table table-stripped table-bordered">
        <thead class="thead-dark">
          <tr>
            <th width="2%">Seial No</th>
            <th width="5%">Student ID</th>
            <th width="10%">Name</th>
            <!-- <th>Course</th> -->
            <th width="5%">Number of classes attended</th>
            <th width="5%">Percentage</th>
            <th width="5%">Marks</th>
          </tr>
        </thead>
        <tbody id="reportTable">

        </tbody>
      </table>
    </div>
  </div>
  <?php include('../footer.php'); ?>
</body>

</html>
