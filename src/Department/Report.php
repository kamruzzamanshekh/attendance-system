<?php
include('header.php');
$page = 'report';
 ?>

<body>
  <?php include('navbar.php'); ?>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#showOrHide').hide();
      $.ajax({
        type: "POST",
        url: "CourseList.php",
        success: function(msg) {
          $('#sel_course_code').html(msg);
        }
      });

      $('#student_report_generate').click(function() {
        $('#showOrHide').show();
        $.ajax({
          type: "POST",
          url: "reportFile.php",
          data: {
            session: $('#session').val(),
            course: $('#sel_course_code').val(),
            marks: $('#Attendance-marks').val()
          },
          success: function(msg) {
            $('#showOrHide').show();
            $('#reportTable').html(msg);


          }
        });
        $.ajax({
          type: "POST",
          url: "reportSheet.php",
          data: {
            session: $('#session').val(),
            course: $('#sel_course_code').val()
          },
          success: function(msg) {
            $('#reportsheet').html(msg);
          }
        });
      });
    });
  </script>
  <section>
    <div id="Report-Genarate-section" class="container">
      <div class="row">

        <div class="col form-group">
          <label for="">Batch</label>
          <select id="batch" class="form-control custom-select" name="" required>
            <?php
            $baseyear=2005;
            $currentyear=date('Y');
            $sub=$currentyear-$baseyear;
            for ($i=$sub-4; $i<=$sub ; $i++) {
              // code...
              	 	echo "<option value='" . $i . "'>" . $i . "</option>";
            }
             ?>
          </select>
        </div>

        <div class="col">
          <label for="">Year</label>
          <select id="year" class="form-control custom-select" name="" required>
            <option value="1st">1<sup>st</sup></option>
            <option value="2nd">2<sup>nd</sup></option>
            <option value="3rd">3<sup>rd</sup></option>
            <option value="4th">4<sup>th</sup></option>
          </select>
        </div>
        <div class="col">
          <label for="">Terms</label>
          <select id="Terms" class="form-control custom-select" name="">
            <option value="1">1-term</option>
            <option value="2">2-term</option>
          </select>
        </div>
      </div>
      <div class="">
        <button id="student_report_generate" type="button" class="btn btn-info btn-lg btn-block mt-5" name="student-report-generate-button">Show Report</button>
      </div>
    </div>
  </section>

  <div class="container" id="showOrHide">
    <div class="border-primary  " id="reportsheet" style='text-align: center;'>
      <!-- calling reportSheet.php -->
    </div>
    <div class="row">
      <table class="table table-stripped table-bordered">
        <thead class="thead-dark">
          <tr>
            <th width="2%">Seial No</th>
            <th width="5%">Student ID</th>
            <th width="10%">Name</th>
            <!-- <th>Course</th> -->
            <th width="5%">Number of classes attended</th>
            <th width="5%">Percentage</th>
            <th width="5%">Marks</th>
          </tr>
        </thead>
        <tbody id="reportTable">

        </tbody>
      </table>
    </div>
  </div>
  <?php include('../footer.php'); ?>
</body>

</html>
